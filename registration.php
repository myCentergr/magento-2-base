<?php
/*
 * Copyright © 2017 myCenter. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MyCenter_Base',
    __DIR__
);
